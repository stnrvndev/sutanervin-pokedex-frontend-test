import configureApiClient from "../configureApiClient";

const pokemonApiClient = configureApiClient('https://pokeapi.co/api/v2');

export const fetchPokemonList = async (limit = 10, offset = 0) => {
    try {
        const response = await pokemonApiClient.get(`/pokemon?limit=${limit}&offset=${offset}`);
        return response.data;
    } catch (error) {
        throw error;
    }
};

export const getPokemonDetailByUrl = async (url) => {
    try {
        const response = await pokemonApiClient.get(url);
        return response.data;
    } catch (error) {
        throw error;
    }
};

export const getPokemonSpeciesByUrl = async (url) => {
    try {
        const response = await pokemonApiClient.get(url);
        return response.data;
    } catch (error) {
        throw error;
    }
};
