import apiClient from './apiClient';

const configureApiClient = (baseURL) => {
  apiClient.defaults.baseURL = baseURL;
  return apiClient;
};

export default configureApiClient;
