import React, { useState } from 'react';
import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import Header from '../components/Header';
import CustomText from '../components/CustomText';
import TypeWrapperCustom from '../components/TypeWrapperCustom';
import ProgressBar from '../components/ProgressBar';
import { typeColor } from '../utils/constants';
import PokeBallBackground from '../components/PokeBallBackground';

const { width: screenWidth, height: screenHeight } = Dimensions.get('window');

const DetailScreen = ({ route, navigation }) => {
  const { pokemon } = route.params;
  const [activeTab, setActiveTab] = useState('About');

  const renderTabContent = () => {
    switch (activeTab) {
      case 'About':
        return (
          <View style={styles.tabContent}>
            <View style={styles.sectionTab}>
              <Text style={styles.sectionTitleText}>Species</Text>
              <Text style={styles.sectionInfoText}>{pokemon.species.name}</Text>
            </View>
            <View style={styles.sectionTab}>
              <Text style={styles.sectionTitleText}>Height</Text>
              <Text style={styles.sectionInfoText}>{pokemon.height / 10} cm</Text>
            </View>
            <View style={styles.sectionTab}>
              <Text style={styles.sectionTitleText}>Weight</Text>
              <Text style={styles.sectionInfoText}>{pokemon.weight / 10} kg</Text>
            </View>
            <View style={styles.sectionTab}>
              <Text style={styles.sectionTitleText}>Abilities</Text>
              <Text style={styles.sectionInfoText}>{pokemon.abilities.map(a => a.ability.name).join(', ')}</Text>
            </View>
            {/*  */}
            <Text style={styles.sectionHeader}>Breeding</Text>
            {/*  */}
            <View style={styles.sectionTab}>
              <Text style={styles.sectionTitleText}>Gender</Text>
              <Text style={styles.sectionInfoText}>{pokemon.gender}</Text>
            </View>
            <View style={styles.sectionTab}>
              <Text style={styles.sectionTitleText}>Egg Groups</Text>
              <Text style={styles.sectionInfoText}>{pokemon.eggGroups}</Text>
            </View>
            <View style={styles.sectionTab}>
              <Text style={styles.sectionTitleText}>Egg Cycle</Text>
              <Text style={styles.sectionInfoText}>{pokemon.eggCycle}</Text>
            </View>
          </View>
        );
      case 'Base Stats':
        return (
          <View style={styles.tabContent}>
            {pokemon.stats.map((stat, index) => (
              <View key={index} style={styles.sectionTab}>
                <Text key={stat.stat.name} style={[styles.sectionTitleText, {flex: 3}]}>{stat.stat.name}</Text>
                <Text key={stat.base_stat} style={[styles.sectionInfoText, {flex: 1}]}>{stat.base_stat}</Text>
                <View style={[styles.sectionInfoText, {flex: 3, alignSelf: 'center'}]}>
                  <ProgressBar value={stat.base_stat}/>
                </View>
              </View>
            ))}
            {/*  */}
            <Text style={styles.sectionHeader}>Type defenses</Text>
            {/*  */}
            <Text style={[styles.sectionTitleText, {fontSize: 14}]}>The effectiveness of each type on Charmander</Text>
          </View>
        );
      case 'Evolution':
        return (
          <View style={styles.tabContent}>
            <Text style={[styles.sectionTitleText, {fontSize: 14, alignSelf: 'center'}]}>Nothing.</Text>
          </View>
        );
      case 'Moves':
        return (
          <View style={styles.tabContent}>
                        <Text style={[styles.sectionTitleText, {fontSize: 14, alignSelf: 'center'}]}>Nothing.</Text>

          </View>
        );
      default:
        return null;
    }
  };

  return (
    <View style={[styles.container, { backgroundColor: typeColor[pokemon.types[0].type.name] || '#fff' }]}>
      <PokeBallBackground style={styles.background} color="#f2f2f2" />
      <Header title="Pokedex" onBackPress={() => navigation.goBack()} onMenuPress={() => () => navigation.goBack()} style={styles.headerContainer} color={'white'}/>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingVertical: 8,
        }}
      >
        <View
          style={{
            flexDirection: 'column',
          }}
        >
          <CustomText fontSize={'largeTitle'} style={styles.pokemonName}>{pokemon.name}</CustomText>
          <TypeWrapperCustom data={pokemon.types} size={'medium'} type={'row'} style={{ paddingHorizontal: 11, padding: 5}} />
        </View>
        <CustomText fontSize={'title'} style={[styles.pokemonName]}>#{pokemon.id.toString().padStart(3, '0')}</CustomText>
      </View>
      <View
        style={{
          marginTop: 12,
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: '100%',
          alignItems: 'center',
          paddingHorizontal: 11,
        }}
      >
        {/* <TypeWrapperCustom data={pokemon.types} size={'medium'} type={'column'} /> */}
      </View>
      <View style={styles.contentContainer}>
        <Image source={{ uri: pokemon.sprites.other['official-artwork'].front_default }} style={styles.image} resizeMode="contain" />
        <View style={styles.infoContainer}>
          <View style={styles.tabContainer}>
            {['About', 'Base Stats', 'Evolution', 'Moves'].map(tab => (
              <TouchableOpacity
                key={tab}
                style={[styles.tab, activeTab === tab && styles.activeTab]}
                onPress={() => setActiveTab(tab)}
              >
                <Text style={activeTab === tab ? styles.tabTextActive : styles.tabText}>{tab}</Text>
              </TouchableOpacity>
            ))}
          </View>
          <ScrollView style={styles.scrollView}>
            {renderTabContent()}
          </ScrollView>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    position: 'absolute',
    top: screenHeight * -0.01,
    right: '10%',
    zIndex: -1,
  },
  backButton: {
    padding: 8,
  },
  headerContainer: {
    paddingHorizontal: 16,
  },
  pokemonName: {
    paddingHorizontal: 16,
    color: '#fff',
  },
  contentContainer: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    marginTop: 150,
    paddingTop: 40,
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  image: {
    width: 200,
    height: 200,
    position: 'absolute',
    top: -150,
  },
  infoContainer: {
    flex: 1,
    width: '100%',
  },
  tabContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  tab: {
    paddingVertical: 10,
    color: 'red',
  },
  activeTab: {
    borderBottomWidth: 2,
    borderBottomColor: '#000',
  },
  sectionTab: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  sectionTitleText: {
    fontSize: 14,
    flex: 1,
    fontWeight: '500',
    color: '#919190',
  },
  sectionInfoText: {
    fontSize: 14,
    flex: 2,
    color: '#000',
  },
  tabTextActive: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000',
  },
  tabText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#919190',
  },
  scrollView: {
    flex: 1,
  },
  tabContent: {
    flex: 1,
  },
  sectionHeader: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 12,
    marginBottom: 10,
    color: '#000',
  },
  infoText: {
    fontSize: 16,
    marginBottom: 5,
    color: '#000',
  },
});

export default DetailScreen;
