import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { View, StyleSheet, Dimensions } from 'react-native';
import Header from '../components/Header';
import PokemonList from '../components/PokemonList';
import { fetchPokemonData } from '../redux/actions/fetchAllPokemon';
import Loading from '../components/Loading';
import CustomText from '../components/CustomText';
import PokeBallBackground from '../components/PokeBallBackground';

const { width: screenWidth, height: screenHeight } = Dimensions.get('window');

const HomeScreen = ({ navigation }) => {
    const data = useSelector((state) => state.pokemon.data);
    const loading = useSelector((state) => state.pokemon.loading);
    const dispatch = useDispatch();
    const [offset, setOffset] = useState(0);
  
    useEffect(() => {
      dispatch(fetchPokemonData(20, offset));
    }, [dispatch, offset]);
  
    const loadMore = () => {
      if (!loading) {
        setOffset(offset + 20);
      }
    };
  
    const handleBackPress = () => {
      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    };
  
    const handleMenuPress = () => {
      console.log('Menu button pressed');
    };
  
    return (
      <View style={styles.container}>
      <PokeBallBackground style={styles.background} color="#e3e3e3" />

        {loading && offset === 0 ? (
          <Loading />
        ) : (
          <PokemonList
            data={data}
            loadMore={loadMore}
            loading={loading}
            navigation={navigation}
            style={styles.pokemonList}
            listHeaderComponent={
              <View>
                <Header
                  title="Pokedex"
                  onBackPress={handleBackPress}
                  onMenuPress={handleMenuPress}
                  style={styles.headerContainer}
                />
                <CustomText
                  fontSize={'largeTitle'}
                  style={styles.headerText}
                >
                  Pokedex
                </CustomText>
              </View>
            }
          />
        )}
      </View>
    );
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    background: {
      position: 'absolute',
      top: screenHeight * -0.25,
      right: screenWidth / 3 - 200,
      zIndex: -1,
    },
    headerContainer: {
      paddingHorizontal: 5
    },
    headerText: {
      marginHorizontal: 5,
      marginBottom: 24,
    },
    pokemonList: {
      paddingHorizontal: 11 // margin between card is 5, so 16 - 5 = 11
    }
  });
  
  export default HomeScreen;
