import React from 'react';
import { Text, StyleSheet } from 'react-native';

const CustomText = ({ children, fontSize, style, ...props }) => {
  const getFontSizeStyle = () => {
    switch (fontSize) {
      case 'largeTitle':
        return styles.largeTitle;
      case 'title':
        return styles.title;
      case 'subtitle':
        return styles.subtitle;
      case 'body':
        return styles.body;
      default:
        return styles.defaultSize;
    }
  };

  return (
    <Text style={[styles.text, getFontSizeStyle(), style]} {...props}>
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  text: {
    color: '#000',
  },
  largeTitle: {
    fontSize: 28,
    fontWeight: 'bold',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  subtitle: {
    fontSize: 18,
    color: '#666',
  },
  body: {
    fontSize: 14,
    color: '#333',
  },
  defaultSize: {
    fontSize: 16,
  },
});

export default CustomText;
