import { View, Text, StyleSheet } from 'react-native';

const TypeWrapperCustom = ({ data, size = 'small', type = 'row', style }) => {
  const typeSize = {
    small: 10,
    medium: 12,
    large: 24,
  };
  
  const orientation = {
    row: { container: styles.typeContainer, typeWrapper: styles.typeWrapper },
    column: { container: styles.typeContainerColumn, typeWrapper: styles.typeWrapperColumn }
  };

  return (
    <View style={[orientation[type].container, style]}>
      {data.map(typeInfo => (
        <View key={typeInfo.slot} style={orientation[type].typeWrapper}>
          <Text
            style={[styles.type, { fontSize: typeSize[size] }]}
          >
            {typeInfo.type.name}
          </Text>
        </View>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  typeContainerColumn: {
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  typeContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  typeWrapper: {
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    paddingHorizontal: 16,
    paddingVertical: 5,
    borderRadius: 10,
    margin: 5,
  },
  typeWrapperColumn: {
    marginBottom: 5,
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    paddingHorizontal: 10,
    paddingVertical: 2,
    borderRadius: 10,
  },
  type: {
    fontSize: 10,
    color: '#fff',
    fontWeight: 'bold',
    textTransform: 'capitalize',
  },
});

export default TypeWrapperCustom;
