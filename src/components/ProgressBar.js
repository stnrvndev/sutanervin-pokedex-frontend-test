import React from 'react';
import { View, StyleSheet } from 'react-native';

const ProgressBar = ({ value }) => {
  const color = value > 60 ? 'green' : 'red';

  return (
    <View key={value} style={styles.progressBarContainer}>
      <View 
        style={[
          styles.progressBar, 
          { width: `${value}%`, backgroundColor: color }
        ]} 
      />
    </View>
  );
};

const styles = StyleSheet.create({
  progressBarContainer: {
    height: 3,
    backgroundColor: '#e0e0e0',
    borderRadius: 5,
    overflow: 'hidden',
    marginVertical: 5,
  },
  progressBar: {
    height: '100%',
    borderRadius: 5,
  },
});

export default ProgressBar;
