import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import CustomText from './CustomText';

const { height } = Dimensions.get('window');
const paddingTopValue = height * 0.03; //
const paddingHorizontalValue = height * 0.03;

const Header = ({ onBackPress, onMenuPress, withTitle = false, titleText, style, color = 'dark' }) => {
  const iconColor = {
    white: '#fff',
    dark: '#000',
  }
  return (
    <View style={[styles.headerContainer, { paddingTop: paddingTopValue }, style]}>
      <View style={styles.header}>
        <TouchableOpacity onPress={onBackPress} style={styles.button}>
          <Icon name="arrow-back" size={24} color={iconColor[color]} />
        </TouchableOpacity>
        <TouchableOpacity onPress={onMenuPress} style={styles.button}>
          <Icon name="menu" size={24} color={iconColor[color]} />
        </TouchableOpacity>
      </View>
      {withTitle && (
          <CustomText fontSize={'title'}>{titleText}</CustomText>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    // paddingHorizontal: paddingHorizontalValue,
    // paddingBottom: paddingHorizontalValue
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // paddingHorizontal: 16,
    paddingVertical: 8,
    elevation: 4,
  },
  button: {
    paddingVertical: 8,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default Header;
