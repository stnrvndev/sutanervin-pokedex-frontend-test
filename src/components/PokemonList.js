import React from 'react';
import { FlatList, StyleSheet, ActivityIndicator, View } from 'react-native';
import PokemonCard from './PokemonCard';
import Loading from './Loading';

const PokemonList = ({ data, loadMore, loading, navigation, style, listHeaderComponent }) => {
  const renderItem = React.useCallback(({ item }) => (
    <PokemonCard
      name={item.name}
      details={item.details}
      navigation={navigation}
    />
  ), [navigation]);

  const renderFooter = React.useCallback(() => {
    if (!loading) return null;
    return <Loading />;
  }, [loading]);

  return (
    <FlatList
      data={data}
      renderItem={renderItem}
      keyExtractor={(item, index) => item.name + index} 
      numColumns={2}
      contentContainerStyle={[styles.list, style]}
      onEndReached={loadMore}
      onEndReachedThreshold={0.5}
      ListHeaderComponent={listHeaderComponent}
      ListFooterComponent={renderFooter}
      initialNumToRender={20}
    />
  );
};

const styles = StyleSheet.create({
  list: {
    // paddingHorizontal: 10,
  },
  loader: {
    marginTop: 10,
    alignItems: 'center',
  },
});

export default PokemonList;
