import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import TypeWrapperCustom from './TypeWrapperCustom';
import { typeColor } from '../utils/constants';

const screenWidth = Dimensions.get('window').width;
const cardHeight = screenWidth * 0.3;
const imageSize = screenWidth * 0.2;

const PokemonCard = React.memo(({ name, details, navigation }) => {
  return (
    <TouchableOpacity onPress={() => navigation.navigate('Detail', { pokemon: details })} style={styles.touchable}>
      <View style={[styles.card, { backgroundColor: typeColor[details.types[0].type.name] || '#fff', height: cardHeight }]}>
        <View style={styles.topContainer}>
          <Text style={styles.name}>{name}</Text>
        </View>
        <Image 
          source={{ uri: details.sprites.other['official-artwork'].front_default }} 
          style={[styles.image, { width: imageSize, height: imageSize }]} 
          resizeMode="cover"
        />
        {details && details.types && (
          <TypeWrapperCustom data={details.types} style={{marginTop: 6}} type='column'/>
        )}
      </View>
    </TouchableOpacity>
  );
});

const styles = StyleSheet.create({
  touchable: {
    flex: 1,
  },
  card: {
    flex: 1,
    borderRadius: 15,
    padding: 10,
    margin: 5,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    elevation: 4,
    shadowColor: '#000',
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { width: 0, height: 2 },
  },
  topContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginTop: '5%',
    alignItems: 'center',
  },
  image: {
    position: 'absolute',
    bottom: 5,
    right: 10,
    maxWidth: '100%',
    maxHeight: '100%',
  },
  name: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
  },
});

export default PokemonCard;
