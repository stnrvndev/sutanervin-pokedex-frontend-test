import * as React from "react";
import Svg, { Path } from "react-native-svg";
import { Dimensions } from "react-native";

const { width, height } = Dimensions.get('window');

function PokeBallBackground({ color, ...props }) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      width={width * 0.8}
      height={height * 0.8}
      fill="none"
      {...props}
    >
      <Path
        d="M22 12c0 5.523-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2s10 4.477 10 10z"
        stroke={color}
        strokeWidth={1.5}
      />
      <Path
        d="M22 12c0 5.523-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2s10 4.477 10 10z"
        stroke={color}
        strokeWidth={1.5}
      />
      <Path
        d="M15 13a3 3 0 11-6 0 3 3 0 016 0zM2 11c2.596 1.004 4.853 1.668 6.998 1.993M22 11.003c-2.593 1.01-4.848 1.675-6.998 1.997"
        stroke={color}
        strokeWidth={1.5}
      />
    </Svg>
  );
}

export default PokeBallBackground;
