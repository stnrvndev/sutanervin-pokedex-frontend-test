import { fetchPokemonList, getPokemonDetailByUrl, getPokemonSpeciesByUrl } from "../../api/services/pokemonApi";

export const fetchDataRequest = () => ({
    type: 'FETCH_DATA_REQUEST',
});

export const fetchDataSuccess = (data) => ({
    type: 'FETCH_DATA_SUCCESS',
    payload: data,
});

export const fetchDataFailure = (error) => ({
    type: 'FETCH_DATA_FAILURE',
    error,
});

export const fetchPokemonData = (limit = 10, offset = 0) => {
    return async (dispatch, getState) => {
        dispatch(fetchDataRequest());
        try {
            const data = await fetchPokemonList(limit, offset);
            const detailedDataPromises = data.results.map(async pokemon => {
              const details = await getPokemonDetailByUrl(pokemon.url);
              const species = await getPokemonSpeciesByUrl(details.species.url);
              
              const maleRatio = 12.5 * (8 - species.gender_rate);
              const femaleRatio = 12.5 * species.gender_rate;
              const gender = `♂ ${maleRatio}% ♀ ${femaleRatio}%`;

              const eggGroups = species.egg_groups.map(group => group.name).join(', ');
              const eggCycle = species.hatch_counter * 257; // Average number of steps per egg cycle

              return { 
                  ...pokemon, 
                  details: { 
                      ...details,
                      gender, 
                      eggGroups, 
                      eggCycle 
                  }
              };
            });
            const detailedData = await Promise.all(detailedDataPromises);

            const currentData = getState().pokemon.data || [];
            const newData = [...currentData, ...detailedData];

            dispatch(fetchDataSuccess(newData));
        } catch (error) {
            dispatch(fetchDataFailure(error));
        }
    };
};
